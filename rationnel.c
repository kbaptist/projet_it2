/*
 *   Ce fichier fait partie d'un projet de programmation donné en Licence 3 
 *   à l'Université de Bordeaux.
 *
 *   Copyright (C) 2015 Giuliana Bianchi, Adrien Boussicault, Thomas Place, Marc Zeitoun
 *
 *    This Library is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This Library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this Library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rationnel.h"
#include "ensemble.h"
#include "automate.h"
#include "parse.h"
#include "scan.h"
#include "outils.h"

#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

int yyparse(Rationnel **rationnel, yyscan_t scanner);

Rationnel *rationnel(Noeud etiquette, char lettre, int position_min, int position_max, void *data, Rationnel *gauche, Rationnel *droit, Rationnel *pere)
{
   Rationnel *rat;
   rat = (Rationnel *) malloc(sizeof(Rationnel));

   rat->etiquette = etiquette;
   rat->lettre = lettre;
   rat->position_min = position_min;
   rat->position_max = position_max;
   rat->data = data;
   rat->gauche = gauche;
   rat->droit = droit;
   rat->pere = pere;
   return rat;
}

Rationnel *Epsilon()
{
   return rationnel(EPSILON, 0, 0, 0, NULL, NULL, NULL, NULL);
}

Rationnel *Lettre(char l)
{
   return rationnel(LETTRE, l, 0, 0, NULL, NULL, NULL, NULL);
}

Rationnel *Union(Rationnel* rat1, Rationnel* rat2)
{
   // Cas particulier où rat1 est vide
   if (!rat1)
      return rat2;

   // Cas particulier où rat2 est vide
   if (!rat2)
      return rat1;
   
   return rationnel(UNION, 0, 0, 0, NULL, rat1, rat2, NULL);
}

Rationnel *Concat(Rationnel* rat1, Rationnel* rat2)
{
   if (!rat1 || !rat2)
      return NULL;

   if (get_etiquette(rat1) == EPSILON)
      return rat2;

   if (get_etiquette(rat2) == EPSILON)
      return rat1;
   
   return rationnel(CONCAT, 0, 0, 0, NULL, rat1, rat2, NULL);
}

Rationnel *Star(Rationnel* rat)
{
   return rationnel(STAR, 0, 0, 0, NULL, rat, NULL, NULL);
}

bool est_racine(Rationnel* rat)
{
   return (rat->pere == NULL);
}

Noeud get_etiquette(Rationnel* rat)
{
   return rat->etiquette;
}

char get_lettre(Rationnel* rat)
{
   return rat->lettre;
}

int get_position_min(Rationnel* rat)
{
   return rat->position_min;
}

int get_position_max(Rationnel* rat)
{
   return rat->position_max;
}

void set_position_min(Rationnel* rat, int valeur)
{
   rat->position_min = valeur;
   return;
}

void set_position_max(Rationnel* rat, int valeur)
{
   rat->position_max = valeur;
   return;
}

Rationnel *fils_gauche(Rationnel* rat)
{
   return rat->gauche;
}

Rationnel *fils_droit(Rationnel* rat)
{
   return rat->droit;
}

Rationnel *fils(Rationnel* rat)
{
   return rat->gauche;
}

Rationnel *pere(Rationnel* rat)
{
   return rat->pere;
}

void print_rationnel(Rationnel* rat)
{
   if (rat == NULL)
   {
      printf("∅");
      return;
   }
   
   switch(get_etiquette(rat))
   {
      case EPSILON:
         printf("ε");         
         break;
         
      case LETTRE:
         printf("%c", get_lettre(rat));
         break;

      case UNION:
         printf("(");
         print_rationnel(fils_gauche(rat));
         printf(" + ");
         print_rationnel(fils_droit(rat));
         printf(")");         
         break;

      case CONCAT:
         printf("[");
         print_rationnel(fils_gauche(rat));
         printf(" . ");
         print_rationnel(fils_droit(rat));
         printf("]");         
         break;

      case STAR:
         printf("{");
         print_rationnel(fils(rat));
         printf("}*");         
         break;

      default:
         assert(false);
         break;
   }
}

Rationnel *expression_to_rationnel(const char *expr)
{
    Rationnel *rat;
    yyscan_t scanner;
    YY_BUFFER_STATE state;

    // Initialisation du scanner
    if (yylex_init(&scanner))
        return NULL;
 
    state = yy_scan_string(expr, scanner);

    // Test si parsing ok.
    if (yyparse(&rat, scanner)) 
        return NULL;
    
    // Libération mémoire
    yy_delete_buffer(state, scanner);
 
    yylex_destroy(scanner);
 
    return rat;
}

void rationnel_to_dot(Rationnel *rat, char* nom_fichier)
{
   FILE *fp = fopen(nom_fichier, "w+");
   rationnel_to_dot_aux(rat, fp, -1, 1);
}

int rationnel_to_dot_aux(Rationnel *rat, FILE *output, int pere, int noeud_courant)
{   
   int saved_pere = noeud_courant;

   if (pere >= 1)
      fprintf(output, "\tnode%d -> node%d;\n", pere, noeud_courant);
   else
      fprintf(output, "digraph G{\n");
   
   switch(get_etiquette(rat))
   {
      case LETTRE:
         fprintf(output, "\tnode%d [label = \"%c-%d\"];\n", noeud_courant, get_lettre(rat), rat->position_min);
         noeud_courant++;
         break;

      case EPSILON:
         fprintf(output, "\tnode%d [label = \"ε-%d\"];\n", noeud_courant, rat->position_min);
         noeud_courant++;
         break;

      case UNION:
         fprintf(output, "\tnode%d [label = \"+ (%d/%d)\"];\n", noeud_courant, rat->position_min, rat->position_max);
         noeud_courant = rationnel_to_dot_aux(fils_gauche(rat), output, noeud_courant, noeud_courant+1);
         noeud_courant = rationnel_to_dot_aux(fils_droit(rat), output, saved_pere, noeud_courant+1);
         break;

      case CONCAT:
         fprintf(output, "\tnode%d [label = \". (%d/%d)\"];\n", noeud_courant, rat->position_min, rat->position_max);
         noeud_courant = rationnel_to_dot_aux(fils_gauche(rat), output, noeud_courant, noeud_courant+1);
         noeud_courant = rationnel_to_dot_aux(fils_droit(rat), output, saved_pere, noeud_courant+1);
         break;

      case STAR:
         fprintf(output, "\tnode%d [label = \"* (%d/%d)\"];\n", noeud_courant, rat->position_min, rat->position_max);
         noeud_courant = rationnel_to_dot_aux(fils(rat), output, noeud_courant, noeud_courant+1);
         break;
         
      default:
         assert(false);
         break;
   }
   if (pere < 0)
      fprintf(output, "}\n");
   return noeud_courant;
}

/* Voir numeroter_rationnel ci-dessous */

void my_numeroter_rationnel(Rationnel * rat, int i){
   switch (get_etiquette(rat)){
      case LETTRE:
         set_position_min(rat, i);
         set_position_max(rat, i);
         break;

      case EPSILON:
         break;

      case STAR:
         my_numeroter_rationnel(fils(rat), i);
         set_position_min(rat, get_position_min(fils(rat)));
         set_position_max(rat, get_position_max(fils(rat)));
         i=get_position_max(fils(rat))+ 1;
         break;

      case CONCAT:
      case UNION :
         my_numeroter_rationnel(fils_gauche(rat), i);
         set_position_min(rat, get_position_min(fils_gauche(rat)));
         i=get_position_max(fils_gauche(rat)) + 1;

         my_numeroter_rationnel(fils_droit(rat), i);
         set_position_max(rat, get_position_max(fils_droit(rat)));
         i=get_position_max(fils_droit(rat)) + 1;
         break;
   }
}

/**
 * @brief @todo Affecte les positions <b>position_min</b> et
 * <b>position_max</b> de toutes les sous-expressions d'une expression
 * rationnelle (voir Rationnel::position_min et
 * Rationnel::position_max pour ces champs, ou la structure Rationnel). 
 * Une expression complète sera numérotée à partir de la position 1 pour sa lettre la plus à gauche.
 * 
 * @brief On parcourt récursivement le rationnel, afin de descendre dans l'arborescence, et trouver les symboles terminaux.
 * Une fois ces symboles trouvés, on les numérotes grâce à un entier passé en paramètre.
 * De cette manière, lorsque l'appel sur le terminal se termine, l'appel du non-terminal parent affecte l'entier servant de compteur avec la valeur de la position max du fils qu'il vient de parcourir.
 * 
 * @param rat Pointeur sur le rationnel.
 */  

void numeroter_rationnel(Rationnel *rat)
{
   int i=1;
   my_numeroter_rationnel(rat, i);
}

/**
 * @brief @todo Teste si le langage d'une expression rationnelle contient le mot vide.
 * 
 * @brief On utilise une implémentation récursive pour parcourir le rationnel, et appliquer notre traitement en fonction de l'étiquette.
 * Une concatenation contient le mot vide si et seulement si sa partie droite ET sa partie gauche contiennent le mot vide.
 * Une union contient le mot vide si au moins un de ses fils contient le mot vide.
 * Une lettre ne contient pas le mot vide.
 * Une étoile ou un epsilon contiennent le mot vide.
 * 
 * @param rat L'expression rationnelle à tester.
 * @return true si le langage de l'expression contient le mot vide.
 */
bool contient_mot_vide(Rationnel *rat){
   switch (get_etiquette(rat)){
      case STAR:
      case EPSILON:
         return true;
         break;
      case LETTRE:
         return false;
         break;
      case CONCAT:
         return (contient_mot_vide(fils_gauche(rat)) && contient_mot_vide(fils_droit(rat)));
         break;
      case UNION:
         return (contient_mot_vide(fils_gauche(rat)) || contient_mot_vide(fils_droit(rat)));
         break;
   }
   return false;
}

/* Voir premier ci-dessous */

Ensemble * my_premier(Rationnel *rat, Ensemble * res){
   switch (get_etiquette(rat)){
      case LETTRE:
         ajouter_element(res, get_position_min(rat));
         break;
      case UNION:
         ajouter_elements(my_premier(fils_gauche(rat), res), res);
         ajouter_elements(my_premier(fils_droit(rat), res), res);
         break;
      case CONCAT:
         ajouter_elements(my_premier(fils_gauche(rat), res), res);
         if (contient_mot_vide(fils_gauche(rat))){
            ajouter_elements(my_premier(fils_droit(rat), res), res);
         }
         break;
      case STAR:
         ajouter_elements(my_premier(fils(rat), res), res);
         break;
      default:
         break;

   }
   return res;
}

/**
 * @brief @todo Calcule l'ensemble des positions des lettres pouvant apparaître comme première lettre d'un mot du langage d'une expression rationnelle.
 * 
 * @brief On utilise une implémentation récursive pour parcourir le rationnel, et appliquer notre traitement en fonction de l'étiquette.
 * On commence par créer l'ensemble res qui contiendra les premiers, puis on applique notre fonction récursive à laquelle on passe res en paramètre.
 * Notre fonction se base sur les règles suivantes :
 * Les premiers d'une lettre (LETTRE) sont cette même lettre.
 * Les premiers d'une union (UNION) sont les premiers de sa partie gauche et de sa partie droite.
 * Les premiers d'une concaténation (CONCAT) sont les premiers de sa partie gauche, et si celle-ci contient le mot vide, alors on ajoute les premiers de la partie droite de la concaténation.
 * Les premiers d'une étoile (STAR) sont les premiers des fils de l'étoile.
 * 
 * @param rat L'expression rationnelle.
 * @return L'ensemble des positions des lettres pouvant apparaître comme première lettre d'un mot du langage de l'expression.
 */
Ensemble *premier(Rationnel *rat)
{
   Ensemble * res = creer_ensemble(NULL, NULL, NULL);
   res = my_premier(rat, res);
   return res;
}

/* Voir dernier ci-dessous */

Ensemble * my_dernier(Rationnel *rat, Ensemble * res){
   switch (get_etiquette(rat)){
      case LETTRE:
         ajouter_element(res, get_position_max(rat));
         break;
      case UNION:
         ajouter_elements(my_dernier(fils_gauche(rat), res), res);
         ajouter_elements(my_dernier(fils_droit(rat), res), res);
         break;
      case CONCAT:
         ajouter_elements(my_dernier(fils_droit(rat), res), res);
         if (contient_mot_vide(fils_droit(rat))){
            ajouter_elements(my_dernier(fils_gauche(rat), res), res);
         }
         break;
      case STAR:
         ajouter_elements(my_dernier(fils(rat), res), res);
         break;
      default:
         break;
   }
   return res;
}


/**
 * @brief @todo Calcule l'ensemble des positions des lettres pouvant apparaître comme dernière lettre d'un mot du langage d'une expression rationnelle.
 *
 * @brief On utilise une implémentation récursive pour parcourir le rationnel, et appliquer notre traitement en fonction de l'étiquette.
 * On commence par créer l'ensemble res qui contiendra les derniers, puis on applique notre fonction récursive à laquelle on passe res en paramètre.
 * Notre fonction se base sur les règles suivantes :
 * Les derniers d'une lettre (LETTRE) sont cette même lettre.
 * Les derniers d'une union (UNION) sont les derniers de sa partie gauche et de sa partie droite.
 * Les derniers d'une concaténation (CONCAT) sont les derniers de sa partie droite, et si celle-ci contient le mot vide, alors on ajoute les derniers de la partie gauche de la concaténation.
 * Les derniers d'une étoile (STAR) sont les derniers des fils de l'étoile.
 * 
 * @param rat L'expression rationnelle.
 * @return L'ensemble des positions des lettres pouvant apparaître comme dernière lettre d'un mot du langage de l'expression.
 */
Ensemble *dernier(Rationnel *rat)
{
   Ensemble * res = creer_ensemble(NULL, NULL, NULL);
   res = my_dernier(rat, res);
   return res;
}

/* Voir suivant ci-dessous */

Ensemble *my_suivant(Rationnel *rat, Ensemble * res, int position){
   Rationnel * rat_pos = rat;
   int i = 0;
   while ((get_etiquette(rat_pos) != LETTRE) && (get_etiquette(rat_pos) != EPSILON)){
      if (position <= get_position_max(fils_gauche(rat_pos))){
         rat_pos = fils_gauche(rat_pos);

      }
      else {
         rat_pos = fils_droit(rat_pos);
      }
      i++;
   }
  
   
   Rationnel * rat2 = rat;

   for (int j = 0; j < i-1; j++){
      if (position <= get_position_max(fils_gauche(rat2))){
         rat2->pere = rat2;
         rat2 = fils_gauche(rat2);
      }
      else {            
         rat2->pere = rat2;
         rat2 = fils_droit(rat2);
      }
   }

   bool boolean = true;

   while ((i>0) && boolean){
      switch (get_etiquette(rat2)){
         case STAR:
         ajouter_elements(res, premier(fils(rat2)));
         break;
         case CONCAT:
         if (position <= get_position_max(fils_gauche(rat2))){
            ajouter_elements(res, premier(fils_droit(rat2)));
         }
         break;
         default:
         break;  
      }

      if (!(est_dans_l_ensemble(dernier(rat2), position)))
         boolean = false;

      rat2 = rat;
      for (int j = 1; j < i-1; j++){
         if (position <= get_position_max(fils_gauche(rat2))){
            rat2->pere = rat2;
            rat2 = fils_gauche(rat2);
         }
         else {            
            rat2->pere = rat2;
            rat2 = fils_droit(rat2);
         }
      }

      i--;
   }
   return res;

}

/**
 * @brief @todo Calcule l'ensemble des positions des lettres pouvant suivre une position donnée dans au moins un mot du langage de l'expression. 
 *
 * @brief
 * On commence par créer l'ensemble res qui contiendra les suivants. On applique ensuite notre algorithme.
 * On incrémente un compteur qui compte le nombre de parent que possède le rationnel recherché.
 * On descend de manière itérative dans le Rationnel passé en paramètre afin de trouver le rationnel (noté dans ce commentaire dest) de position l'entier passé en paramètre. 
 * Une fois qu'on "est" sur le rationnel voulu, on remonte d'un cran dans l'arborescence. Ce rationnel est stocké rat2.
 * 
 * Tant que dest fait partie des derniers de rat2, on ajoute à res des éléments suivant ces règles :
 * Si rat2 est une étoile, alors on ajoute les premiers du Rationnel sous l'étoile.
 * Si rat2 est une concaténation, alors si dest est dans le fils gauche, on ajoute le premier du fils droit de la concaténation.
 *                                      si dest est dans le fils droit, on ne peut rien ajouter pour le moment.
 * Si rat 2 est une union, on ne peut rien ajouter non plus, car on ne peut pas savoir ce qui suit.
 * On teste alors si dest fait partie des derniers de rat2, auquel cas on considère que l'itération prend fin.
 * Sinon on remonte rat2 dans l'arborescence, et recommence l'itération.
 * 
 * 
 * @param rat Une expression rationnelle.
 * @param position Un entier désignant une position dans l'expression rationnelle.
 * @return L'ensemble des positions des lettres pouvant suivre la position donnée en second argument dans au moins un mot du langage de l'expression.
 */

Ensemble *suivant(Rationnel *rat, int position)
{
   assert(!(position<0));
   Ensemble * res = creer_ensemble(NULL, NULL, NULL);
   if (position > get_position_max(rat)){
      return res;
   }

   res = my_suivant(rat, res, position);
   return res;
}

/* Renvoie un pointeur vers la lettre à la position position dans le rationnel rat. */
Rationnel *trouver_position(Rationnel *rat, int position){
   Rationnel * rat_pos = rat;
   while ((get_etiquette(rat_pos) != LETTRE) && (get_etiquette(rat_pos) != EPSILON)){
      if (position <= get_position_max(fils_gauche(rat_pos))){
         rat_pos = fils_gauche(rat_pos);

      }
      else {
         rat_pos = fils_droit(rat_pos);
      }
   }
   return rat_pos;
}


/**
 * @brief @todo
 * @brief Retourne l'automate de Glushkov associé à une expression rationnelle.
 * 
 * 
 * @brief On commence par numéroter le rationnel rat passé en paramètre, et on mémorise les ensembles Premier (contenant les premiers de rat) et Dernier (contenant les derniers de rat). 
 * Puis on crée un automate res auquel on ajoute un état 0 initial.
 * Ensuite, on ajoute les états à l'automate res: un état dont l'indice est contenu dans Dernier sera un état final.
 * Un état dont l'indice est contenu dans Premier sera lié (par une transition dans l'automate) à l'état 0 (qui sera le seul état initial de res).
 * Enfin, on ajoute les transitions à l'automate, en appelant pour chaque état la fonction suivant.
 * 
 * 
 * @param rat Une expression rationnelle.
 * @return L'automate de Glushkov associé à l'expression rationnelle. Ses états seront numérotés par des entiers commençant à 0, l'état initial.
 */
Automate *Glushkov(Rationnel *rat)
{
   Automate * res = creer_automate();
   numeroter_rationnel(rat);

   Ensemble * Premier = premier(rat);
   Ensemble * Dernier = dernier(rat);
   int nb_etats = get_position_max(rat) + 1;

   ajouter_etat_initial(res, 0);
   if (contient_mot_vide(rat))
      ajouter_etat_final(res, 0);

   for (int i = 1; i < nb_etats; i++){
      ajouter_etat(res, i);
      if (est_dans_l_ensemble(Dernier, i)){
         ajouter_etat_final(res, i);
      }
      if (est_dans_l_ensemble(Premier, i)){
         ajouter_transition(res, 0, get_lettre(trouver_position(rat, i)), i);
      }
      for (int j = 1; j < nb_etats; j++){
         if (est_dans_l_ensemble(suivant(rat, i), j)){
            ajouter_transition(res, i, get_lettre(trouver_position(rat, j)), j);
         }
      }

   }
   res = automate_accessible(creer_automate_minimal(res));
   return res;
}

/* Teste si deux rationnels reconnaissent le même langage.*/
bool meme_langage_rationnel (Rationnel * rat1, Rationnel * rat2)
{
   if (automates_reconnaissent_le_meme_language(Glushkov(rat1), Glushkov(rat2)))
      return true;
   return false;
}


/**
 * @brief @todo
 * Teste si deux expressions reconnaissent le même langage.
 * 
 * @brief On passe les chaînes de caractères en rationnels.
 * Puis on applique Glushkov sur ces deux rationnels pour obtenir deux automates, dont on va tester l'équivalence des langages grâce à la fonction automates_reconnaissent_le_meme_langage.
 *
 * @param expr1 La première expression.
 * @param expr2 La deuxième expression.
 * @result true ou false.
 */
bool meme_langage (const char *expr1, const char* expr2)
{
   return meme_langage_rationnel(expression_to_rationnel(expr1), expression_to_rationnel(expr2));
}


void remplir_systeme(int origine, char lettre, int fin, Systeme systeme){
// L'union est utlisée pour remédier au cas où plusieurs transitions existent entre l'état origine et l'état fin.
   systeme[origine][fin] = Union(systeme[origine][fin], Lettre(lettre));
}


/**
 * @brief @todo Construit le système d'équations de langages associé à un automate. Voir @ref Systeme pour la représentation de ce système.
 * 
 * @brief On commence par allouer la mémoire au système créé (nombre de colonnes = nombre de lignes +1; nombre de lignes = nombres d'états de l'automate).
 * Puis pour chaque transition de l'automate, on appelle la fonction remplir_systeme qui va remplir la case du systeme correspondante.
 * 
 * @param automate L'automate à transformer en système, en supposant ses états
 * numérotés de 0 à n-1.
 * @return Le système d'équations de langages associé à l'automate.
 */
Systeme systeme(Automate *automate)
{
   int nb_lignes = taille_ensemble(get_etats(automate));
   int nb_colonnes = nb_lignes + 1;
   Systeme res = malloc(sizeof(Rationnel**)*nb_lignes);
   
   for (int i = 0; i < nb_lignes; i++){
      res[i] = malloc(sizeof(Rationnel*)*nb_colonnes);
      for (int j = 0; j < nb_colonnes; j++){
         res[i][j] = NULL;
      }
      if (est_un_etat_final_de_l_automate(automate, i)){
         res[i][nb_colonnes-1] = Epsilon();
      }
   }
   pour_toute_transition(automate, (void*)remplir_systeme, res);
   return res;
}

void print_ligne(Rationnel **ligne, int n)
{
   for (int j = 0; j <=n; j++)
      {
         print_rationnel(ligne[j]);
         if (j<n)
            printf("X%d\t+\t", j);
      }
   printf("\n");
}

void print_systeme(Systeme systeme, int n)
{
   for (int i = 0; i <= n-1; i++)
   {
      printf("X%d\t= ", i);
      print_ligne(systeme[i], n);
   }
}


/**
 * @brief @todo Résout une équation de langages en utilisant le lemme d'Arden: si \f$U\f$ et \f$V\f$ sont deux langages et \f$U\f$ ne contient pas le mot vide, et si \f$X\f$ est un langage tel que \f$X=UX+V\f$, alors \f$X=U^*V\f$. 
 * * 
 * @brief Si ligne[numero_variable] contient le mot vide ou est NULL, la ligne ne change et est retournée intacte.
 * Sinon, ligne[numero_variable] devient NULL, et on modifie chacune des autres cases de la ligne.
 * 
 * @param ligne La ligne codant l'équation de langages.
 * @param numero_variable Le numéro de la variable jouant le rôle de X dans le lemme d'Arden, à exprimer en fonction des autres variables. Si ce numéro est \f$i\f$, le membre gauche de l'équation est donc \f$X_i\f$ (ce membre gauche n'est pas codé dans le paramètre 'ligne').
 * @param nb_vars Le nombre de variables.
 * @return Une ligne codant l'équation résultat, dans laquelle la variable de numéro 'numero_variable' a été exprimée en fonction des autres.
 *
 * Par exemple, si 'nb_vars' vaut 3, il y a 3 variables \f$X_0\f$, \f$X_1\f$ et \f$X_2\f$. Si en plus l'argument 'numero_variable' vaut 1 et l'argument 'ligne' vaut \f$\{a, b, \sf{NULL},\varepsilon \}\f$, alors l'équation représentée par ces arguments est \f[X_1 = a\cdot X_0 + b\cdot X_1 + \emptyset\cdot X_2 + \varepsilon\f] ou encore \f[X_1 = a\cdot X_0 + b\cdot X_1 +  \varepsilon.\f] Le lemme d'Arden donne \f[X_1 = b^*(a\cdot X_0+\varepsilon)=b^*a\cdot X_0+b^*.\f] La fonction doit donc retourner la ligne \f[\{b^*a, {\sf NULL}, {\sf NULL}, b^* \}.\f]
 */
Rationnel **resoudre_variable_arden(Rationnel **ligne, int numero_variable, int n)
{
   if (ligne[numero_variable] == NULL)
      return ligne;
   if (contient_mot_vide(ligne[numero_variable]))
      return ligne;
   Rationnel * tmp = ligne[numero_variable];
   ligne[numero_variable] = NULL;
   for (int i = 0; i < n+1; i++){
      ligne[i] = Concat(Star(tmp), ligne[i]);
   }
   return ligne;
}

/**
 * @brief @todo Substitue dans une ligne une variable par sa valeur en fonction des autres variables.`
 * 
 * @brief Pour cela, on parcourt toutes les cases de la ligne passée en paramètre pour changer leur valeur.
 * Ligne[numero_variable] devient NULL, les autres valeurs sont modifiées en fonction de valeur_variable[i] et de l'ancienne valeur de ligne[numero_variable].
 * 
 * @param ligne La ligne dans laquelle effectuer la substitution.
 * @param numero_variable Le numéro de la variable à substituer par une valeur dépendant des autres variables.
 * @param valeur_variable La valeur, en fonction des autres variables, par laquelle remplacer dans la ligne donnée en premier argument la variable dont le numéro est donné en second argument.
 * @return La ligne résultant de la substitution dans 'ligne' de la variable de numéro 'numero_variable' par la valeur 'valeur_variable'.
*/
Rationnel **substituer_variable(Rationnel **ligne, int numero_variable, Rationnel **valeur_variable, int n)
{
   Rationnel * tmp = ligne[numero_variable];
   ligne[numero_variable] = NULL;
   for (int i = 0; i < n+1; i++){
      ligne[i] = Union( ligne[i], Concat(tmp, valeur_variable[i]));
   }
   return ligne;
}

/* 
Utilisée dans la fonction resoudre_systeme.
Renvoie true si le systeme est réduit au maximum, et false sinon.
Un systeme est réduit au maximum si les seules valeurs non NULL se trouvent dans la dernière colonne (CAD les cases systeme[i][n+1]).
*/
bool systeme_reduit_au_max(Systeme systeme, int n){
   for (int j = 0; j < n; j++){
      for(int i = 0; i < n; i++){
         if (systeme[i][j] != NULL)
            return false;
      }
   }
   return true;
}

/**
 * @brief @todo Résout un système d'équations de langages.
 * 
 * @brief On se sert d'une fonction resoudre_systeme pour savoir si le systeme est réduit au maximum.
 * Tant que ce n'est pas le cas: on parcourt chaque ligne du système et, lorsque cela est nécessaire, on résoud l'équation d'Arden (grâce à la fonction resoudre_variable_arden), et on substitue la variable dans chaque autre ligne (grâce à la fonction substituer_variable).
 * 
 * @param sys Le système à résoudre.
 * @param nb_vars Le nombre de variables.
 * @return Le système d'équations représentant la valeur de chaque variable comme un langage rationnel.
 */
Systeme resoudre_systeme(Systeme systeme, int n)
{
   while(!(systeme_reduit_au_max(systeme, n))){

      for (int i = 0; i < n; i++){
         if(systeme[i][i] != NULL){
            systeme[i] = resoudre_variable_arden(systeme[i], i, n);
         }
         for(int j = 0; j < n; j++){
            if ((j != i) && (systeme[j][i] != NULL)){
               systeme[j] = substituer_variable(systeme[j], i, systeme[i], n);
            }
            
         }
      }
   }


   return systeme;
}


/**
 * @brief @todo Convertit un automate en expression rationnelle.
 * 
 * @brief On commence par créer un système dérivé de l'automate, grâce à la fonction systeme. Puis on résoud ce système en appelant la fonction resoudre_systeme.
 * On obtient donc le système résolu. Il nous suffit ensuite de récupérer les valeurs contenues dans la derniere colonne de chaque ligne i, avec i un état initial de l'automate passé en paramètre.
 * On stocke toutes ces valeurs dans un rationnel nommé res, initialisé à Epsilon (on ajoute chaque valeur grçace à une Union).
 * Le rationnel res est ensuite retourné par la fonction: il contient l'expression rationnelle décrivant le langage reconnu par l'automate.
 * 
 * @param automate L'automate d'entrée.
 * @return Une expression rationnelle décrivant le langage reconnu par l'automate.
 */
Rationnel *Arden(Automate *automate)
{
   int nb_etat = taille_ensemble(get_etats(automate));
   Systeme sys = systeme(automate);

   sys = resoudre_systeme(sys,  nb_etat);
  

   Rationnel * res = Epsilon();
      
   
   for (int i = 0; i <= nb_etat ; ++i){
      numeroter_rationnel(res);
      if(est_un_etat_initial_de_l_automate(automate, i)){
         if(get_position_max(res)==0){
            res = sys[i][nb_etat];
         }
         else{
            res = Union(res, sys[i][nb_etat]);
         }
      }
   }

   return res;
}

