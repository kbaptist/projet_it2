#include <automate.h>
#include <rationnel.h>
#include <ensemble.h>
#include <outils.h>
#include <parse.h>
#include <scan.h>

#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>


/** 
  * @brief Nous transformons une expression en Rationnel rat.
  * @brief rat est transformé en Automate grâce à Glushkov
  * @brief Cet Automate est ensuite remis en Rationnel nommé res grâce à Arden 
  * @brief Les langages reconnus par res et rat sont ensuite comparés.
  */
void my_test(char * expression){

	Rationnel * rat;
  rat = expression_to_rationnel(expression);

  Automate * automate = Glushkov(rat);

  printf("\n\nArden\n");
  Rationnel * res = Arden(automate);

  if (meme_langage_rationnel(res, rat))
  {
    printf("C'est bon :-)\n");
  }

  else
  {
    printf("\n");
    printf("\n");
    printf("\n");
    printf("C'est pas bon :-(\n");
    printf("\n");
    printf("\n");
    printf("\n");
    printf("\n");
  }

}

int main (){

    my_test("a");
    my_test("a*");
    my_test("a.b");
    my_test("a+b");
    my_test("a*.b");
    my_test("(a+b)*");
    my_test("(a*.b)*");
    my_test("a+(b)*.a*");
    my_test("((a*.b)*.c)*.d");

    return 1;
}